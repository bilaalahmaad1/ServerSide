var express = require('express'),
  cors = require('cors')
app = express()
port = process.env.PORT || 3000,
  mongoose = require('mongoose'),
  Country = require('./api/models/countryModel'), //created model loading here
  City = require('./api/models/cityModel');
Area = require('./api/models/areaModel');
Cuisine = require('./api/models/cuisineModel');
Category = require('./api/models/categoryModel');
subCategory = require('./api/models/subCategoryModel');
IngredientType = require('./api/models/ingredientTypeModel');
IngredientList = require('./api/models/ingredientListModel');
VendorDetail = require('./api/models/vendorDetailModel');
Admin = require('./api/models/admin');
Item=require('./api/models/itemModel')
Item=require('./api/models/orderModel')

bodyParser = require('body-parser');
var jwt = require('jsonwebtoken');
var config = require('./config')
// mongoose instance connection url connection
mongoose.Promise = global.Promise;
mongoose.connect(config.database);
app.set('superSecret', config.secret);

var allowCrossDomain = function (req, res, next) {
  res.header('Access-Control-Allow-Origin', '*');
  res.header('Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE');
  res.header('Access-Control-Allow-Headers', 'Content-Type');
  //  res.header('Access-Control-Allow-Headers','Authentication');
  // res.header('Access-Control-Allow-Headers','X-Requested-With,content-type,Authentication');

  res.setHeader('Access-Control-Allow-Headers', 'X-Requested-With,content-type,Authorization,Accept');


  next();
};


app.use(cors(), function (req, res, next) {
  console.log(req._parsedUrl.path);

  if (req._parsedUrl.path === "/login"
  || req._parsedUrl.path === "/user"
  || req._parsedUrl.path ==="/usercities" 
  ||req._parsedUrl.path ==="/userareas"
  ||req._parsedUrl.path ==="/uservendordetails"||req._parsedUrl.path ==="/itemsusers") {

    console.log("admin pathh checking" + req._parsedUrl.path);

    next();
  }
  else { 
    
    let token = req.headers.authorization;


    if (token) {

      jwt.verify(token, app.get('superSecret'), (err, decoded) => {

        if (err) {
          console.log("error in token");
          
          return res.send({ success: false, error: err });
        } else {

          console.log("decodes in else of error");
          console.log(decoded);
          req.decoded = decoded;

          next();

        }
      });

    } else {
      return res.send({
        success: false,
        message: 'No token provided.'
      });
      

    }

  }

});



app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());
app.use(allowCrossDomain);

var routes = require('./api/routes/Routes'); //importing route
//var routes =require('./api/routes/cityRoutes');
routes(app); //register the route

app.listen(port);

console.log(' RESTful API server started on: ' + port);
