'use strict';
var mongoose = require('mongoose');
var Schema = mongoose.Schema;


var vendorDetails = new Schema({
    vendorName: {
        type: String,
        required: 'Kindly enter the name of the vendorName'
    },

    email: {
        type: String,
        required: 'Kindly enter the name of the email'
    },
    password: {
        type: String,
        required: 'Kindly enter the name of the password'
    },
    confirmPassword: {
        type: String,
        required: 'Kindly enter the name of the confirmPassword'
    },
    userName: {
        type: String,
        required: 'Kindly enter the name of the userName'
    },
    vendorSMSnumber: {
        type: String,
        required: 'Kindly enter the name of the vendorSMSnumber'
    },
    contactAddress: {
        type: String,
        required: 'Kindly enter the name of the contactAddress'
    },
    // vendorCallNumber: {
    //     type: String,
    //     required: 'Kindly enter the name of the vendorCallNumber'
    // },
    categories: [{
        type: String,
        required: 'Kindly enter the name of the categories'
    }],
    cusines:[ {
        type: String,
        required: 'Kindly enter the name of the cusines'
    }],
    city: {
        type: String,
        required: 'Kindly enter the name of the city'
    },
    area: {
        type: String,
        required: 'Kindly enter the name of the area'
    },
    deliveryLocation: [{
        type: String,
        required: 'Kindly enter the name of the deliveryLocation'
    }],
    commision: {
        type: Number,
        required: 'Kindly enter the name of the commision'
    },
    serviceTax: {
        type: String,
        required: 'Kindly enter the name of the serviceTax'
    },
    minOrderValue: {
        type: String,
        required: 'Kindly enter the name of the minOrderValue'
    },
    deliveryTime: {
        type: String,
        required: 'Kindly enter the name of the deliveryTime'
    },
    paymentOption: {
        type: String,
        required: 'Kindly enter the name of the paymentOption'
    },
    resturantType: {
        type: String,
        required: 'Kindly enter the name of the resturantType'
    },
    deliveryCostAmount: {
        type: String,
        required: 'Kindly enter the name of the deliveryCostAmount'
    },
    deliveryCostKM: {
        type: String,
        required: 'Kindly enter the name of the deliveryCostKM'
    },
    deliveryAdditionalCostKM: {
        type: String,
        required: 'Kindly enter the name of the deliveryAdditionalCostKM'
    },
    vendorStatus: {
        type: String,
        required: 'Kindly enter the name of the vendorStatus'
    },
    type:{
        type: String,
        required: 'Kindly enter the name of the type'

    },
   

    Created_date: {
        type: Date,
        default: Date.now
    },
   
    status: {
        type: [{
            type: String,
            enum: ['pending', 'ongoing', 'completed']
        }],
        default: ['pending']
    }
});





module.exports = mongoose.model('vendorDetails', vendorDetails);