'use strict';
var mongoose = require('mongoose');
var Schema = mongoose.Schema;


var Cuisines = new Schema({
  name: {
    type: String,
    required: 'Kindly enter the name of the name'
  },
  sortOrder:{
      type: String,
    required: 'Kindly enter the name of the sortOrder'
  },cusineStatus:{
    type: String,
    required: 'Kindly enter the name of the cusineStatus'
  },
  Created_date: {
    type: Date,
    default: Date.now
  },
  status: {
    type: [{
      type: String,
      enum: ['pending', 'ongoing', 'completed']
    }],
    default: ['pending']
  }
});

module.exports = mongoose.model('Cuisines', Cuisines);