'use strict';
var mongoose = require('mongoose');
var Schema = mongoose.Schema;


var subCategories = new Schema({
    name: {
        type: String,
        required: 'Kindly enter the name of the name'
    },
    parentCategory: {
        type: String,
        required: 'Kindly enter the name of the parentCategory'
    },
    sucCategoryStatus:{   
         type: String,
        required: 'Kindly enter the name of the sucCategoryStatus'},
    Created_date: {
        type: Date,
        default: Date.now
    },
    status: {
        type: [{
            type: String,
            enum: ['pending', 'ongoing', 'completed']
        }],
        default: ['pending']
    }
});

module.exports = mongoose.model('subCategories', subCategories);