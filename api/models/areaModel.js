'use strict';
var mongoose = require('mongoose');
var Schema = mongoose.Schema;


var Areas = new Schema({
  name: {
    type: String,
    required: 'Kindly enter the name of the name'
  },
  countryName:{
      type: String,
    required: 'Kindly enter the name of the countryName'
  },
  cityName:{
       type: String,
    required: 'Kindly enter the name of the cityName'
  },
  areaStatus:{
    type: String,
    required: 'Kindly enter the name of areaStatus'
  },
  Created_date: {
    type: Date,
    default: Date.now
  },
  status: {
    type: [{
      type: String,
      enum: ['pending', 'ongoing', 'completed']
    }],
    default: ['pending']
  }
});

module.exports = mongoose.model('Areas', Areas);