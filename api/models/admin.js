'use strict';
var mongoose = require('mongoose');
var Schema = mongoose.Schema;


var Admin = new Schema({
  userName: { 
    type: String 
  },
  address: { 
    type: String 
  },
  phone: { 
    type: String 
  },

  password: {
    type: String
  },
  email: {
    type: String
  },
  type:{
    type:String
  }
  
});

module.exports = mongoose.model('Admin', Admin);