'use strict';
var mongoose = require('mongoose');
var Schema = mongoose.Schema;


var Countries = new Schema({
  name: {
    type: String,
    required: 'Kindly enter the name of the name'
  },
  ISOcode:{
      type: String,
    required: 'Kindly enter the name of the IsoCode'
  },
  countryStatus:{
    type: String,
    required: 'Kindly enter the name of the CountryStatus'
  },
  Created_date: {
    type: Date,
    default: Date.now
  },
  
  status: {
    type: [{
      type: String,
      enum: ['pending', 'ongoing', 'completed']
    }],
    default: ['pending']
  }
});

module.exports = mongoose.model('Countries', Countries);