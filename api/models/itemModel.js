'use strict';
var mongoose = require('mongoose');
var Schema = mongoose.Schema;


var Item = new Schema({
  category: {
    type: String,
    required: 'Kindly enter the name of the category'

  },
  cuisine: {
    type: String,
    required: 'Kindly enter the name of the cuisine'
  },
  subCategory: {
    type: String,
    required: 'Kindly enter the name of the subCategory'
  },

  itemName: {
    type: String,
    required: 'Kindly enter the name of the itemName'
  },
  itemPricePerUnit: {
    type: String,
    required: 'Kindly enter the name of the itemPricePerUnit'
  },
  ingredientListNames:[ { 
    type: String ,
    // required: 'Kindly enter the name of the ingredientListNames'
  }],
  ingredientListPrices:[ {
     type: String
// ,     required: 'Kindly enter the name of the ingredientListPrices'
     }],
     listIngredientName:{
      type: String
     },
  listIngredienttype: {
    type: String
  },
  vendorName: {
    type: String,
    required: 'Kindly enter the name of the vendorName'
  },
  itemStatus: {
    type: String,
    required: 'Kindly enter the name of the itemStatus'
  },
  approval: {
    type: String,
    required: 'Kindly enter the name of the approval'
  }

});

module.exports = mongoose.model('Item', Item);