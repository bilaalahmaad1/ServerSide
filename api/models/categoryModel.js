'use strict';
var mongoose = require('mongoose');
var Schema = mongoose.Schema;


var Categories = new Schema({
  name: {
    type: String,
    required: 'Kindly enter the name of the name'
  },
  Created_date: {
    type: Date,
    default: Date.now
  },
  categoryStatus:{
    type: String,
    required: 'Kindly enter the name of the categoryStatus'
  },
  status: {
    type: [{
      type: String,
      enum: ['pending', 'ongoing', 'completed']
    }],
    default: ['pending']
  }
});

module.exports = mongoose.model('Categories', Categories);