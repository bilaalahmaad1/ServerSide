'use strict';
var mongoose = require('mongoose');
var Schema = mongoose.Schema;


var Cities = new Schema({
  name: {
    type: String,
    required: 'Kindly enter the name of the cityname'
  },
  countryName:{
      type: String,
    required: 'Kindly enter the name of the countryname'
  },
  cityStatus:{
    type: String,
    required: 'Kindly enter the name of CityStatus'
  },
  Created_date: {
    type: Date,
    default: Date.now
  },
  status: {
    type: [{
      type: String,
      enum: ['pending', 'ongoing', 'completed']
    }],
    default: ['pending']
  }
});

module.exports = mongoose.model('Cities', Cities);