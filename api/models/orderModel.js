'use strict';
var mongoose = require('mongoose');
var Schema = mongoose.Schema;


var Orders = new Schema({
  shopName: {
    type: String,
    required: 'Kindly enter the name of the shopName'
  },
  customerName:{
      type: String,
    required: 'Kindly enter the name of the customerName'
  },
  orderTotal:{
    type: String,
    required: 'Kindly enter the name of orderTotal'
  },
  paymentStatus:{
    type: String,
    required: 'Kindly enter the name of paymentStatus'
  },
  dileveryStatus:{
    type: String,
    required: 'Kindly enter the name of dileveryStatus'
  },
  Created_date: {
    type: Date,
    default: Date.now
  },
  status: {
    type: [{
      type: String,
      enum: ['pending', 'ongoing', 'completed']
    }],
    default: ['pending']
  }
});

module.exports = mongoose.model('Orders', Orders);