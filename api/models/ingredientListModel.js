'use strict';
var mongoose = require('mongoose');
var Schema = mongoose.Schema;


var IngredientLists = new Schema({
    ingredienttypename:{
        type: String,
    },
    names: [{
        type: String,
        // required: 'Kindly enter the name of the name'
    }],
    prices:[{
        type: String,
        // required: 'Kindly enter the name of the prices'
    }],
    IngredientType: {
        type: String,
        // required: 'Kindly enter the name of the IngredientType'
    },
    IngredientListStatus:{
        type: String,
        // required: 'Kindly enter the name of the IngredientListStatus'
    },
    Created_date: {
        type: Date,
        default: Date.now
    },
    status: {
        type: [{
            type: String,
            enum: ['pending', 'ongoing', 'completed']
        }],
        default: ['pending']
    }
});

module.exports = mongoose.model('IngredientLists', IngredientLists);