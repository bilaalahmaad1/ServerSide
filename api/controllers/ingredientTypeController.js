'use strict';


var mongoose = require('mongoose'),
  IngredientType = mongoose.model('IngredientTypes');

exports.list_all_ingredienttypes = function(req, res) {
  IngredientType.find({}, function(err, ingredienttype) {
    if (err)
      res.send(err);
    res.json(ingredienttype);
  });
};




exports.create_a_ingredienttype = function(req, res) {
  var new_ingredienttype = new IngredientType(req.body);
  new_ingredienttype.save(function(err, ingredienttype) {
    if (err)
      res.send(err);
    res.json(ingredienttype);
  });
};


exports.read_a_ingredienttype = function(req, res) {
  IngredientType.findById(req.params.ingredienttypeId, function(err, ingredienttype) {
    if (err)
      res.send(err);
    res.json(ingredienttype);
  });
};


exports.update_a_ingredienttype = function(req, res) {
  IngredientType.findOneAndUpdate({_id: req.params.ingredienttypeId}, req.body, {new: true}, function(err, ingredienttype) {
    if (err)
      res.send(err);
    res.json(ingredienttype);
  });
};


exports.delete_a_ingredienttype = function(req, res) {


  IngredientType.remove({
    _id: req.params.ingredienttypeId
  }, function(err, ingredienttype) {
    if (err)
      res.send(err);
    res.json({ message: 'IngredientType successfully deleted' });
  });
};
