'use strict';


var mongoose = require('mongoose'),
  IngredientList = mongoose.model('IngredientLists');

exports.list_all_ingredientlists = function(req, res) {
  IngredientList.find({}, function(err, ingredientlist) {
    if (err)
      res.send(err);
    res.json(ingredientlist);
  });
};




exports.create_a_ingredientlist = function(req, res) {
  var new_ingredientlist = new IngredientList(req.body);
  new_ingredientlist.save(function(err, ingredientlist) {
    if (err)
      res.send(err);
    res.json(ingredientlist);
  });
};


exports.read_a_ingredientlist = function(req, res) {
  IngredientList.findById(req.params.ingredientlistId, function(err, ingredientlist) {
    if (err)
      res.send(err);
    res.json(ingredientlist);
  });
};


exports.update_a_ingredientlist = function(req, res) {
  IngredientList.findOneAndUpdate({_id: req.params.ingredientlistId}, req.body, {new: true}, function(err, ingredientlist) {
    if (err)
      res.send(err);
    res.json(ingredientlist);
  });
};


exports.delete_a_ingredientlist = function(req, res) {


  IngredientList.remove({
    _id: req.params.ingredientlistId
  }, function(err, ingredientlist) {
    if (err)
      res.send(err);

    res.json({ message: 'Ingredientlistsuccessfully deleted' });
  });
};
