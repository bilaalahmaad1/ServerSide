'use strict';


var mongoose = require('mongoose'),
  vendorDetail = mongoose.model('vendorDetails');

exports.list_all_vendordetails = function(req, res) {
  vendorDetail.find({}, function(err, vendordetail) {
    if (err)
      res.send(err);
    res.json(vendordetail);
  });
};




exports.create_a_vendordetail = function(req, res) {
  var new_vendordetail = new vendorDetail(req.body);
  new_vendordetail.save(function(err, vendordetail) {
    if (err)
      res.send(err);
    res.json(vendordetail);
  });
};


exports.read_a_vendordetail = function(req, res) {
  vendorDetail.findById(req.params.vendordetailId, function(err, vendordetail) {
    if (err)
      res.send(err);
    res.json(vendordetail);
  });
};


exports.update_a_vendordetail = function(req, res) {
  vendorDetail.findOneAndUpdate({_id: req.params.vendordetailId}, req.body, {new: true}, function(err,vendordetail) {
    if (err)
      res.send(err);
    res.json(vendordetail);
  });
};


exports.delete_a_vendordetail = function(req, res) {


  vendorDetail.remove({
    _id: req.params.vendordetailId
  }, function(err, vendordetail) {
    if (err)
      res.send(err);
    res.json({ message: 'vendordetail successfully deleted' });
  });
};
