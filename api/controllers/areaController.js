'use strict';


var mongoose = require('mongoose'),
  Area = mongoose.model('Areas');

exports.list_all_areas = function(req, res) {
  Area.find({}, function(err, area) {
    if (err)
      res.send(err);
    res.json(area);
  });
};




exports.create_a_area = function(req, res) {
  var new_area = new Area(req.body);
  new_area.save(function(err, area) {
    if (err)
      res.send(err);
    res.json(area);
  });
};


exports.read_a_area = function(req, res) {
  Area.findById(req.params.areaId, function(err, area) {
    if (err)
      res.send(err);
    res.json(area);
  });
};


exports.update_a_area = function(req, res) {
  Area.findOneAndUpdate({_id: req.params.areaId}, req.body, {new: true}, function(err, area) {
    if (err)
      res.send(err);
    res.json(area);
  });
};


exports.delete_a_area = function(req, res) {


  Area.remove({
    _id: req.params.areaId
  }, function(err, area) {
    if (err)
      res.send(err);
    res.json({ message: 'area successfully deleted' });
  });
};
