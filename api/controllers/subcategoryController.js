'use strict';


var mongoose = require('mongoose'),
  subCategory = mongoose.model('subCategories');

exports.list_all_subcategories = function(req, res) {
  subCategory.find({}, function(err, subcategory) {
    if (err)
      res.send(err);
    res.json(subcategory);
  });
};




exports.create_a_subcategory = function(req, res) {
  var new_subcategory = new subCategory(req.body);
  new_subcategory.save(function(err, subcategory) {
    if (err)
      res.send(err);
    res.json(subcategory);
  });
};


exports.read_a_subcategory = function(req, res) {
  subCategory.findById(req.params.subcategoryId, function(err, subcategory) {
    if (err)
      res.send(err);
    res.json(subcategory);
  });
};


exports.update_a_subcategory = function(req, res) {
  subCategory.findOneAndUpdate({_id: req.params.subcategoryId}, req.body, {new: true}, function(err,subcategory) {
    if (err)
      res.send(err);
    res.json(subcategory);
  });
};


exports.delete_a_subcategory = function(req, res) {


  subCategory.remove({
    _id: req.params.subcategoryId
  }, function(err, subcategory) {
    if (err)
      res.send(err);
    res.json({ message: 'subcategory successfully deleted' });
  });
};
