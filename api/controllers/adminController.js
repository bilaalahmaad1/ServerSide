'use strict';


var mongoose = require('mongoose'),
    Admin = mongoose.model('Admin');

var mongoose = require('mongoose'),
    vendorDetail = mongoose.model('vendorDetails');
var jwt = require('jsonwebtoken');

exports.list_all_admins = function (req, res) {
    Admin.find({}, function (err, admin) {
        if (err)
            res.send(err);
        res.json(admin);
    });
};




exports.create_a_admin = function (req, res) {
    var new_admin = new Admin(req.body);
    new_admin.save(function (err, admin) {
        if (err)
            res.send(err);
        res.json(admin);
    });
};


exports.read_a_admin = function (req, res) {
    Admin.findById(req.params.adminId, function (err, admin) {
        if (err)
            res.send(err);
        res.json(admin);
    });
};


exports.update_a_admin = function (req, res) {
    Admin.findOneAndUpdate({ _id: req.params.adminId }, req.body, { new: true }, function (err, admin) {
        if (err)
            res.send(err);
        res.json(admin);
    });
};


exports.delete_a_admin = function (req, res) {


    Admin.remove({
        _id: req.params.adminId
    }, function (err, admin) {
        if (err)
            res.send(err);
        res.json({ message: 'admin successfully deleted' });
    });
};



exports.generate_token = function (req, res) {
    
   


    Admin.findOne({ email: req.body.email }, function (err, admin) {
        if (err)
            res.send(err);
        if (admin) {
              console.log( "this is admin"+admin)
            
            // console.log( "this is admin"+tpye)
            var token = jwt.sign({ admin }, app.get('superSecret'), {
                expiresIn: 4500

            });

            res.json({
                admin,
                token,
                success: true,
                message: 'Authentication success. Correct email this is an admin.'
            });
        }
        else {
            console.log("no user");

            // res.json({
            //     success: false,
            //     message: 'Authentication failed. Wrong email. this email is not an admin'
            // });

            vendorDetail.findOne({ email: req.body.email }).exec( function (err, vendor) {
                if (err){           
                     res.send(err);
                    }
                
                if(vendor) {
                    //     //  console.log(" user")
                    var token = jwt.sign({ vendor }, app.get('superSecret'), {
                        expiresIn: 4500
        
                    });
        
                    res.json({
                        vendor,
                        token,
                        success: true,
                        message: 'Authentication success. Correct email.this is vendor'
                    });
                }
                else {
                    res.json({
                        success: false,
                        message: 'Authentication failed. Wrong email.'
                    });
                }
        
        
            });
        }
    });



    

};


exports.user = function (req, res) {
   console.log("i am in user function controler")
   let token = req.headers.authorization;
            jwt.verify(token, app.get('superSecret'), (err, user) => {

                if (err) {
                    console.log("error in token");

                    res.json({ success: false, error: err });
                } else {
                      
                    res.json({
                        user,
                        // success: true, error: err
                    });
                }
            });

       
 
};

