'use strict';


var mongoose = require('mongoose'),
  Cuisine = mongoose.model('Cuisines');

exports.list_all_cuisines= function(req, res) {
  Cuisine.find({}, function(err, cuisine) {
    if (err)
      res.send(err);
    res.json(cuisine);
  });
};




exports.create_a_cuisine = function(req, res) {
  var new_cuisine = new Cuisine(req.body);
  new_cuisine.save(function(err, cuisine) {
    if (err)
      res.send(err);
    res.json(cuisine);
  });
};


exports.read_a_cuisine = function(req, res) {
  Cuisine.findById(req.params.cuisineId, function(err, cuisine) {
    if (err)
      res.send(err);
    res.json(cuisine);
  });
};


exports.update_a_cuisine = function(req, res) {
  Cuisine.findOneAndUpdate({_id: req.params.cuisineId}, req.body, {new: true}, function(err, cuisine) {
    if (err)
      res.send(err);
    res.json(cuisine);
  });
};


exports.delete_a_cuisine= function(req, res) {


  Cuisine.remove({
    _id: req.params.cuisineId
  }, function(err, cuisine) {
    if (err)
      res.send(err);
    res.json({ message: 'cuisine successfully deleted' });
  });
};
