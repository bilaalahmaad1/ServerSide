'use strict';


var mongoose = require('mongoose'),
  City = mongoose.model('Cities');

exports.list_all_cities = function(req, res) {
  City.find({}, function(err, city) {
    if (err)
      res.send(err);
    res.json(city);
  });
};




exports.create_a_city = function(req, res) {
  var new_city = new City(req.body);
  new_city.save(function(err, city) {
    if (err)
      res.send(err);
    res.json(city);
  });
};


exports.read_a_city = function(req, res) {
  City.findById(req.params.cityId, function(err, city) {
    if (err)
      res.send(err);
    res.json(city);
  });
};


exports.update_a_city = function(req, res) {
  City.findOneAndUpdate({_id: req.params.cityId}, req.body, {new: true}, function(err, city) {
    if (err)
      res.send(err);
    res.json(city);
  });
};


exports.delete_a_city = function(req, res) {


  City.remove({
    _id: req.params.cityId
  }, function(err, city) {
    if (err)
      res.send(err);
    res.json({ message: 'city successfully deleted' });
  });
};
