'use strict';
module.exports = function (app) {
  var countryList = require('../controllers/countryController');
  var cityList = require('../controllers/cityController');
  var areaList = require('../controllers/areaController');
  var cuisineList = require('../controllers/cuisineController');
  var categoryList = require('../controllers/categoryController');
  var subCategoryList = require('../controllers/subcategoryController');
  var ingredientTypeList = require('../controllers/ingredientTypeController');
  var ingredientListList = require('../controllers/ingredientListController');
  var vendorDetailList = require('../controllers/vendorDetailsController');
  var adminList =require('../controllers/adminController')
  var itemList =require('../controllers/itemController')
  var orderList =require('../controllers/orderController')

  // todoList Routes
  app.route('/countries')
    .get(countryList.list_all_countries)
    .post(countryList.create_a_country);


  app.route('/countries/:countryId')
    .get(countryList.read_a_country)
    .put(countryList.update_a_country)
    .delete(countryList.delete_a_country);

    app.route('/cities')
    .get(cityList.list_all_cities)
    .post(cityList.create_a_city);

    app.route('/usercities')
    .get(cityList.list_all_cities);


  app.route('/cities/:cityId')
    .get(cityList.read_a_city)
    .put(cityList.update_a_city)
    .delete(cityList.delete_a_city);

    app.route('/areas')
    .get(areaList.list_all_areas)
    .post(areaList.create_a_area);
    app.route('/userareas')
    .get(areaList.list_all_areas);










  app.route('/areas/:areaId')
    .get(areaList.read_a_area)
    .put(areaList.update_a_area)
    .delete(areaList.delete_a_area);


  app.route('/cuisines')
    .get(cuisineList.list_all_cuisines)
    .post(cuisineList.create_a_cuisine);


  app.route('/cuisines/:cuisineId')
    .get(cuisineList.read_a_cuisine)
    .put(cuisineList.update_a_cuisine)
    .delete(cuisineList.delete_a_cuisine);


  app.route('/categories')
    .get(categoryList.list_all_categories)
    .post(categoryList.create_a_category);


  app.route('/categories/:categoryId')
    .get(categoryList.read_a_category)
    .put(categoryList.update_a_category)
    .delete(categoryList.delete_a_category);


  app.route('/subcategories')
    .get(subCategoryList.list_all_subcategories)
    .post(subCategoryList.create_a_subcategory);


  app.route('/subcategories/:subcategoryId')
    .get(subCategoryList.read_a_subcategory)
    .put(subCategoryList.update_a_subcategory)
    .delete(subCategoryList.delete_a_subcategory);

    app.route('/ingredienttypes')
    .get(ingredientTypeList.list_all_ingredienttypes)
    .post(ingredientTypeList.create_a_ingredienttype);


  app.route('/ingredienttypes/:ingredienttypeId')
    .get(ingredientTypeList.read_a_ingredienttype)
    .put(ingredientTypeList.update_a_ingredienttype)
    .delete(ingredientTypeList.delete_a_ingredienttype);

    app.route('/ingredientlists')
    .get(ingredientListList.list_all_ingredientlists)
    .post(ingredientListList.create_a_ingredientlist);


  app.route('/ingredientlists/:ingredientlistId')
    .get(ingredientListList.read_a_ingredientlist)
    .put(ingredientListList.update_a_ingredientlist)
    .delete(ingredientListList.delete_a_ingredientlist);

    app.route('/vendordetails')
    .get(vendorDetailList.list_all_vendordetails)
    .post(vendorDetailList.create_a_vendordetail);


  app.route('/vendordetails/:vendordetailId')
    .get(vendorDetailList.read_a_vendordetail)
    .put(vendorDetailList.update_a_vendordetail)
    .delete(vendorDetailList.delete_a_vendordetail);
    
  app.route('/uservendordetails')
    .get(vendorDetailList.list_all_vendordetails);

    app.route('/items')
    .get(itemList.list_all_items)
    .post(itemList.create_a_item);
    

    app.route('/itemsusers')
    .get(itemList.list_all_items)


  app.route('/items/:itemId')
    .get(itemList.read_a_item)
    .put(itemList.update_a_item)
    .delete(itemList.delete_a_item);

    app.route('/orders')
    .get(orderList.list_all_orders)
    .post(orderList.create_a_order);


  app.route('/orders/:orderId')
    .get(orderList.read_a_order)
    .put(orderList.update_a_order)
    .delete(orderList.delete_a_order);


    app.route('/admins')
    .get(adminList.list_all_admins)
    .post(adminList.create_a_admin);


  app.route('/admins/:adminId')
    .get(adminList.read_a_admin)
    .put(adminList.update_a_admin)
    .delete(adminList.delete_a_admin);


    app.route('/login')
    .post(adminList.generate_token);

    app.route('/user')
    .get(adminList.user);
    
    

};
